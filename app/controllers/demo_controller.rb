class DemoController < ApplicationController
  require 'httparty'
  require 'json'

  def get_posts
    @base_url = "http://192.168.10.160:3000"
    @token = "connection_token"
    url = "#{@base_url}/api/v1/posts?token=#{@token}"
    resp = HTTParty.get(url)
    @json_resp = JSON.parse(resp.body)
  end
end
